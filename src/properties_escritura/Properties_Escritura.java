/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package properties_escritura;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author Sebastian Gonzalez Burdiles.
 */
public class Properties_Escritura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Properties propiedades = new Properties();
        OutputStream salida = null;
        try {
            salida = new FileOutputStream("configuracion.properties");
            //asignamos los valores a las propiedades
            propiedades.setProperty("variabledemo", "codehero");
            propiedades.setProperty("variabletest", "test");
            //guardamos el archivo de propiedades en la carpeta de aplicacion
            propiedades.store(salida, null);
            salida.close();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
